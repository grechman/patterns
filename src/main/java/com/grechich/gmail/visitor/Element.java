package com.grechich.gmail.visitor;

public interface Element {
    void accept(Visitor visitor);
}
