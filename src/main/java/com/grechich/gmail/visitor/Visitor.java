package com.grechich.gmail.visitor;

public interface Visitor {
    void visit(Car car);
    void visit(Bike bike);
}
