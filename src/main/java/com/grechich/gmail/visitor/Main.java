package com.grechich.gmail.visitor;

public class Main {
    public static void main(String[] args) {
        Car car = new Car();
        Bike bike = new Bike();
        BadDriver badDriver = new BadDriver();
        GoodDriver goodDriver = new GoodDriver();
        goodDriver.visit(car);
        badDriver.visit(car);
        goodDriver.visit(bike);
        badDriver.visit(bike);
    }
}
