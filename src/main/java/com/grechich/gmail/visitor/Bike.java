package com.grechich.gmail.visitor;

public class Bike implements Element{
    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }
}
