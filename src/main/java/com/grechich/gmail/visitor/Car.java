package com.grechich.gmail.visitor;

public class Car implements Element{
    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }
}
