package com.grechich.gmail.facade;

public class Belt {
    public boolean isBeltOn = false;
    public void passBeltOn(){
        isBeltOn = true;
        System.out.println("Belt is on");
    }
}
