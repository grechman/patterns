package com.grechich.gmail.facade;

public class Engine {
    public boolean isOn = false;
    public void Start(){
        isOn = true;
        System.out.println("Engine is on");
    }

}
