package com.grechich.gmail.facade;

public class Lights {
    public boolean isLightsOn = false;
    public void lightsOn(){
        isLightsOn = true;
        System.out.println("Lights is on");
    }
}
