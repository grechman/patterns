package com.grechich.gmail.facade;

public class Driver {
    private Engine engine = new Engine();
    private Belt belt = new Belt();
    private Lights lights = new Lights();


    public void drive(){
        if (!engine.isOn)
            engine.Start();
        if (!belt.isBeltOn)
            belt.passBeltOn();
        if (!lights.isLightsOn)
            lights.lightsOn();
        System.out.println("Im driving");
    }
}
