package com.grechich.gmail.command;

public class Main {
    public static void main(String[] args) {
        Car car = new Car();
        Driver driver = new Driver(new StartCommand(car), new StopCommand(car), new BeepCommand(car));
        driver.start();
        driver.stop();
        driver.beep();
    }
}
