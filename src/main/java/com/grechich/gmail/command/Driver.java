package com.grechich.gmail.command;

public class Driver {
    StartCommand startCommand;
    StopCommand stopCommand;
    BeepCommand beepCommand;

    public Driver(StartCommand startCommand, StopCommand stopCommand, BeepCommand beepCommand) {
        this.startCommand = startCommand;
        this.stopCommand = stopCommand;
        this.beepCommand = beepCommand;
    }

    public void start(){
        startCommand.execute();
    }
    public void stop(){
        stopCommand.execute();
    }
    public void beep(){
        beepCommand.execute();
    }
}
