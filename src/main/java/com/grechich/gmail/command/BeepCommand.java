package com.grechich.gmail.command;

public class BeepCommand implements Command{
    Car car;

    public BeepCommand(Car car) {
        this.car = car;
    }

    @Override
    public void execute() {
        car.beep();
    }
}
