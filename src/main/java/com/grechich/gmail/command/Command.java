package com.grechich.gmail.command;

public interface Command {
    void execute();
}
