package com.grechich.gmail.command;

public class Car {
    public void start(){
        System.out.println("Started!");
    }
    public void stop(){
        System.out.println("Stopped!");
    }
    public void beep(){
        System.out.println("Beep!!!");
    }
}
