package com.grechich.gmail.command;

public class StopCommand implements Command{
    Car car;

    public StopCommand(Car car) {
        this.car = car;
    }

    @Override
    public void execute() {
        car.stop();
    }
}
