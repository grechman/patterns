package com.grechich.gmail.iterator;

public interface Iterator {
    boolean hasNext();
    Object next();
    Object current();
    Object first();
    Object last();


}
