package com.grechich.gmail.iterator;

public interface CollectionIter {
    Iterator getIterator();

}
