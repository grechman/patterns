package com.grechich.gmail.iterator;

public class Castings implements CollectionIter{
    private String[] castings = {"Frog", "Core", "Bottom"};
    @Override
    public Iterator getIterator() {
        return new FirstIter(castings);
    }
}
