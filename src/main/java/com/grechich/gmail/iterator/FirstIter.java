package com.grechich.gmail.iterator;

public class FirstIter implements Iterator {
    private int index;
    private Object[] objects;

    public FirstIter(Object[] objects) {
        this.index = 0;
        this.objects = objects;
    }

    @Override
    public boolean hasNext() {
        if (index<objects.length)
            return true;
        return false;
    }

    @Override
    public Object next() {
        return objects[index++];
    }

    @Override
    public Object current() {
        return objects[index];
    }

    @Override
    public Object first() {
        return objects[0];
    }

    @Override
    public Object last() {
        return objects[objects.length-1];
    }
}
