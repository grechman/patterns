package com.grechich.gmail.delegate;

public class Cola implements Drink{
    @Override
    public void filling() {
        System.out.println("Filling with Cola drink");
    }
}
