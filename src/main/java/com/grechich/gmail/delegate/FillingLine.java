package com.grechich.gmail.delegate;

public class FillingLine {
    private Drink drink;
    public void setDrink(Drink drink){
        this.drink = drink;
    }
    public void filling(){
        drink.filling();
    }
}
