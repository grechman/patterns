package com.grechich.gmail.delegate;

public class Main {
    public static void main(String[] args) {
        FillingLine line = new FillingLine();
        line.setDrink(new Cola());
        line.filling();
        line.setDrink(new Water());
        line.filling();
    }
}
