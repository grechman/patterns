package com.grechich.gmail.bridge;

public abstract class Car {
    private TypeOfCar typeOfCar;

    public Car(TypeOfCar typeOfCar) {
        this.typeOfCar = typeOfCar;
    }

    abstract void showDetails();
    public TypeOfCar typeOfCar(){
        return typeOfCar;
    }


}
