package com.grechich.gmail.bridge;

public class Sedan implements TypeOfCar{
    @Override
    public void showTypeOfCar() {
        System.out.println("Sedan");
    }
}
