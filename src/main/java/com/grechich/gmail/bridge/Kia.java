package com.grechich.gmail.bridge;

public class Kia extends Car{
    public Kia(TypeOfCar typeOfCar) {
        super(typeOfCar);
    }

    @Override
    public void showDetails(){
        System.out.println("Kia");
        typeOfCar().showTypeOfCar();
    }
}
