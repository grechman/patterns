package com.grechich.gmail.bridge;

public class Hatchback implements TypeOfCar{
    @Override
    public void showTypeOfCar() {
        System.out.println("Hatchback");
    }
}
