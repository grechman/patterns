package com.grechich.gmail.bridge;

public class BMW extends Car{
    public BMW(TypeOfCar typeOfCar) {
        super(typeOfCar);
    }

    @Override
    public void showDetails(){
        System.out.println("BMW");
        typeOfCar().showTypeOfCar();
    }
}
