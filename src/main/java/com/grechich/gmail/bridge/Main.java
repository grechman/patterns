package com.grechich.gmail.bridge;

public class Main {
    public static void main(String[] args) {
        Car car = new BMW(new Hatchback());
        car.showDetails();
    }

}
