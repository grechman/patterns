package com.grechich.gmail.composite;

public class BlackPencil implements Drawer{

    @Override
    public void draw() {
        System.out.println("Draw black line!");
    }
}
