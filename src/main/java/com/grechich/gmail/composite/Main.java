package com.grechich.gmail.composite;

public class Main {
    public static void main(String[] args) {

        PencilCase drawer = new PencilCase();
        PencilCase drawer1 = new PencilCase();
        Drawer blackPencil = new BlackPencil();
        Drawer blackPencil2 = new BlackPencil();
        Drawer red = new RedPencil();
        drawer1.addDrawer(blackPencil);
        drawer1.addDrawer(blackPencil);
        drawer1.addDrawer(red);
        drawer.addDrawer(drawer1);
        drawer.addDrawer(drawer1);
        drawer.addDrawer(blackPencil2);
        drawer.draw();

    }
}
