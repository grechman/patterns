package com.grechich.gmail.composite;

public class YellowPencil implements Drawer{
    @Override
    public void draw() {
        System.out.println("Draw yellow line!");
    }
}
