package com.grechich.gmail.composite;

public class RedPencil implements Drawer{
    @Override
    public void draw() {
        System.out.println("Draw red line!");
    }
}
