package com.grechich.gmail.composite;

public interface Drawer {
    void draw();
}
