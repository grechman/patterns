package com.grechich.gmail.composite;

import java.util.ArrayList;
import java.util.List;

public class PencilCase implements Drawer{

    private List<Drawer> drawers = new ArrayList<>();

    public void addDrawer (Drawer drawer){
        drawers.add(drawer);
    }

    public void deleteDrawer(Drawer drawer){
        drawers.remove(drawer);
    }

    @Override
    public void draw() {
        for (Drawer drawer: drawers){
            drawer.draw();
        }
    }
}
