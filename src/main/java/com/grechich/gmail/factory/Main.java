package com.grechich.gmail.factory;

public class Main {
    public static void main(String[] args) {
        String shopType = "Atb";
        ShopFactory factory = new ShopFactory();
        Shop shop = factory.getShop(shopType);
        shop.showYouSlogan();
    }
}
