package com.grechich.gmail.factory;

import java.util.Locale;

public class ShopFactory {

    public ShopFactory() {
    }

    public Shop getShop(String shopType) {
        Shop shop;
        if ("SILPO".equals(shopType.toUpperCase(Locale.ROOT))){
            shop=new Silpo();
        }else if ("ATB".equals(shopType.toUpperCase(Locale.ROOT))) {
            shop = new Atb();
        }else {
            System.out.println("Wrong type of shop. Shop is null!!!");
            shop = null;
        }
        return shop;
    }
}

