package com.grechich.gmail.abstractFactory;

import com.grechich.gmail.abstractFactory.bear.Bear;
import com.grechich.gmail.abstractFactory.bear.DarkBear;
import com.grechich.gmail.abstractFactory.bear.LightBear;
import com.grechich.gmail.abstractFactory.chocolate.Chocolate;
import com.grechich.gmail.abstractFactory.chocolate.DarkChocolate;
import com.grechich.gmail.abstractFactory.chocolate.MilkChocolate;

public class LightFactory implements FoodFactory {
    @Override
    public Bear getBear() {
        return new LightBear();
    }

    @Override
    public Chocolate getChocolate() {
        return new MilkChocolate();
    }
}
