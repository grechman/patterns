package com.grechich.gmail.abstractFactory.bear;

public class DarkBear extends Bear{
    @Override
    public void drink() {
        System.out.println("Dark bear is drinking!");
    }
}
