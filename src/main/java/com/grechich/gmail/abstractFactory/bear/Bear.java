package com.grechich.gmail.abstractFactory.bear;

public abstract class Bear {
    public abstract void drink();
}
