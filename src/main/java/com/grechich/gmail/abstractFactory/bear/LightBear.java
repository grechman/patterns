package com.grechich.gmail.abstractFactory.bear;

public class LightBear extends Bear{
    @Override
    public void drink() {
        System.out.println("Light bear is drinking!");
    }
}
