package com.grechich.gmail.abstractFactory.chocolate;

public class DarkChocolate extends Chocolate{

    @Override
    public void taste() {
        System.out.println("Dark chocolate is good!!");
    }
}
