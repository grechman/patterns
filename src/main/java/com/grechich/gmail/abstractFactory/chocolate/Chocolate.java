package com.grechich.gmail.abstractFactory.chocolate;

public abstract class Chocolate {
    public abstract void taste();
}
