package com.grechich.gmail.abstractFactory.chocolate;

public class MilkChocolate extends Chocolate{

    @Override
    public void taste() {
        System.out.println("Milk chocolate is good!!");
    }
}
