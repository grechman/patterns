package com.grechich.gmail.abstractFactory;

import com.grechich.gmail.abstractFactory.bear.Bear;
import com.grechich.gmail.abstractFactory.chocolate.Chocolate;

public interface FoodFactory {
    Bear getBear();
    Chocolate getChocolate();
}
