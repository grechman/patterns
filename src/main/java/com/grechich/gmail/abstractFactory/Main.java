package com.grechich.gmail.abstractFactory;

public class Main {
    public static void main(String[] args) {
        FoodFactory factory = getFactory("light");
        factory.getBear().drink();
        factory.getChocolate().taste();

    }

    private static FoodFactory getFactory(String foodType) {
        switch (foodType) {
            case "dark":
                return new DarkFactory();
            case "light":
                return new LightFactory();
            default:
                throw new RuntimeException("Wrong type of factory - " + foodType);
        }

    }
}
