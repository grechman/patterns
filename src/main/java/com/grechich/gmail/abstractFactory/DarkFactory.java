package com.grechich.gmail.abstractFactory;

import com.grechich.gmail.abstractFactory.bear.Bear;
import com.grechich.gmail.abstractFactory.bear.DarkBear;
import com.grechich.gmail.abstractFactory.chocolate.Chocolate;
import com.grechich.gmail.abstractFactory.chocolate.DarkChocolate;

public class DarkFactory implements FoodFactory {
    @Override
    public Bear getBear( ) {
        return new DarkBear();
    }

    @Override
    public Chocolate getChocolate() {
        return new  DarkChocolate();
    }
}
