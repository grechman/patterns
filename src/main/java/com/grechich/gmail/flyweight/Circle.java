package com.grechich.gmail.flyweight;

public class Circle implements Shape {
    private int r = 5;

    @Override
    public void draw(int x, int y) {
        System.out.println("Circle " + r);
    }
}
