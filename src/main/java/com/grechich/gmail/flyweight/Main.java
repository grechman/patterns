package com.grechich.gmail.flyweight;

public class Main {
    public static void main(String[] args) {
        ShapeFactory factory = new ShapeFactory();
        Shape circle = factory.getShape("square");
        circle.draw(1,2);
    }
}
