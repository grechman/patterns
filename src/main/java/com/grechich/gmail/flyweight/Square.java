package com.grechich.gmail.flyweight;

public class Square implements Shape{
    @Override
    public void draw(int x, int y) {
        System.out.println("Square");
    }
}
