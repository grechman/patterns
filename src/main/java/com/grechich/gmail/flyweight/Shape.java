package com.grechich.gmail.flyweight;

//Flyweight
public interface Shape {
    void draw(int x, int y);
}
