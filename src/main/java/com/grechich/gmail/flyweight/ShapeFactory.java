package com.grechich.gmail.flyweight;

import java.util.HashMap;
import java.util.Map;

public class ShapeFactory {
    private Map<String, Shape> shapes = new HashMap<>();
    private Shape shape;

    public Shape getShape(String name) {
        shape = shapes.get(name);
        if (shape == null) {
            switch (name) {
                case "circle":
                    return new Circle();
                case "square":
                    return new Square();
                case "point":
                    return new Point();
            }
        }
        return shape;
    }

}
