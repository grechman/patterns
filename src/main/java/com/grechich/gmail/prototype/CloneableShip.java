package com.grechich.gmail.prototype;

public interface CloneableShip {
    Ship clone();
}
