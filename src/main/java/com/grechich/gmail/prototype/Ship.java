package com.grechich.gmail.prototype;

public class Ship implements CloneableShip{

    private int age;
    private String name;

    public Ship(int age, String name) {
        this.age = age;
        this.name = name;
    }

    @Override
    public Ship clone() {
        Ship clone = new Ship(age, name);
        return clone;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Ship{" +
                "age=" + age +
                ", name='" + name + '\'' +
                '}';
    }
}
