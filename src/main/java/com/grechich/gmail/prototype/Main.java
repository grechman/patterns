package com.grechich.gmail.prototype;

public class Main {
    public static void main(String[] args) {

        Ship ship = new Ship(12, "Alex");
        System.out.println(ship);
        Ship ship1 = ship.clone();
        System.out.println(ship1);
        ship.setAge(88);
        System.out.println(ship);
        System.out.println(ship1);
        System.out.println("_______________________________");

    }
}
