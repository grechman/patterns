package com.grechich.gmail.observer;

public class Main {
    public static void main(String[] args) {

        Alert alert = new Alert();
        Phone phone = new Phone();
        Siren siren = new Siren();
        alert.addObserver(phone);
        alert.addObserver(siren);
        alert.alertOn();
        alert.alertOff();
        alert.alertOn();
        alert.alertOff();
    }
}
