package com.grechich.gmail.observer;

public interface Observer {
    void handleEvent(boolean isAlert);
}
