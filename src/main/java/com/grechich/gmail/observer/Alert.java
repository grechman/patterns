package com.grechich.gmail.observer;

import java.util.ArrayList;
import java.util.List;

public class Alert implements Observed{
    private List<Observer> observers = new ArrayList<>();
    private boolean isAlert = false;

    public void alertOn(){
        isAlert = true;
        notifyObservers();
    }
    public void alertOff(){
        isAlert = false;
        notifyObservers();
    }


    @Override
    public void addObserver(Observer observer) {
        observers.add(observer);
    }

    @Override
    public void deleteObserver(Observer observer) {
        observers.remove(observer);
    }

    @Override
    public void notifyObservers() {
        for (Observer observer: observers){
            observer.handleEvent(isAlert);
        }
    }
}
