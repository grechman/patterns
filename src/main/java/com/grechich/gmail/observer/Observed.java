package com.grechich.gmail.observer;

public interface Observed {
    void addObserver(Observer observer);
    void deleteObserver(Observer observer);
    void notifyObservers();
}
