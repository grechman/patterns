package com.grechich.gmail.observer;

public class Phone implements Observer{
    @Override
    public void handleEvent(boolean isAlert) {
        if (isAlert){
            System.out.println("Alert!!!");
        }else {
            System.out.println("Alert canceled");
        }
    }
}
