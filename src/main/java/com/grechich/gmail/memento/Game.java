package com.grechich.gmail.memento;

public class Game {
    private String level;
    private int ms;

    public void set(String level, int ms) {
        this.level = level;
        this.ms = ms;
    }

    public Save createSave(){
        Save save = new Save(level, ms);
        return save;
    }

    public void loadGame(Save save){
        level = save.getLevel();
        ms = save.getMs();
    }

    @Override
    public String toString() {
        return "Game{" +
                "level='" + level + '\'' +
                ", ms=" + ms +
                '}';
    }
}
