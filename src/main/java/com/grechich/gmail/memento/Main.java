package com.grechich.gmail.memento;

public class Main {
    public static void main(String[] args) {
        Game game = new Game();
        game.set("1", 1234);
        System.out.println(game);

        File file = new File();
        file.setSave(game.createSave());

        game.set("2", 123156);
        System.out.println(game);

        game.loadGame(file.getSave());
        System.out.println(game);
    }
}
