package com.grechich.gmail.adapter;

public class CutKnifeAdapter extends KnifeCutter implements Cutter{
    @Override
    public void cut() {
        cutWithKnife();
    }
}
