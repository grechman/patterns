package com.grechich.gmail.templateMethod;

public class Main {
    public static void main(String[] args) {
        AbstractClass a = new ClassA();
        AbstractClass b = new ClassB();
        a.print();
        b.print();
    }
}
