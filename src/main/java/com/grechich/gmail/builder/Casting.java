package com.grechich.gmail.builder;

public class Casting {
    private String steelMark;
    private double weight;
    private int drawing;

    public void setSteelMark(String steelMark) {
        this.steelMark = steelMark;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public void setDrawing(int drawing) {
        this.drawing = drawing;
    }

    @Override
    public String toString() {
        return "Casting{" +
                "steelMark='" + steelMark + '\'' +
                ", weight=" + weight +
                ", drawing=" + drawing +
                '}';
    }
}
