package com.grechich.gmail.builder;

public class Main {
    public static void main(String[] args) {
        Director director = new Director(new FrogBuilder());
        Casting casting = director.buildCasting();
        System.out.println(casting);

        director.setBuilder(new KlemBuilder());
        casting = director.buildCasting();
        System.out.println(casting);

    }
}
