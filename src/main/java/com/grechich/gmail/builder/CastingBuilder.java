package com.grechich.gmail.builder;

public abstract class CastingBuilder {
    public Casting casting = new Casting();

    public abstract void buildSteelMark();
    public abstract void buildWeight();
    public abstract void buildDrawing();

    public Casting getCasting(){
        return casting;
    }
}
