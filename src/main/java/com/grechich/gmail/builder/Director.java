package com.grechich.gmail.builder;

public class Director {
    private CastingBuilder builder;
    public Director(CastingBuilder builder) {
        this.builder = builder;
    }


    public void setBuilder(CastingBuilder cb){
        this.builder=cb;
    }
    public Casting buildCasting(){
        builder.buildDrawing();
        builder.buildWeight();
        builder.buildSteelMark();
        return builder.getCasting();
    }

}
