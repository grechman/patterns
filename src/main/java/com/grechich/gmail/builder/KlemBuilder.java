package com.grechich.gmail.builder;

public class KlemBuilder extends CastingBuilder {

    @Override
    public void buildSteelMark() {
        casting.setSteelMark("35L");
    }

    @Override
    public void buildWeight() {
        casting.setWeight(0.15);
    }

    @Override
    public void buildDrawing() {
        casting.setDrawing(31564);
    }
}
