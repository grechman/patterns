package com.grechich.gmail.builder;

public class FrogBuilder extends CastingBuilder {

    @Override
    public void buildSteelMark() {
        casting.setSteelMark("110G13L");
    }

    @Override
    public void buildWeight() {
        casting.setWeight(315);
    }

    @Override
    public void buildDrawing() {
        casting.setDrawing(31546);
    }
}
