package com.grechich.gmail.mediator;

public interface User {
    void sendMsg(String msg);
    void getMsg(String msg);
}
