package com.grechich.gmail.mediator;

public class UserImpl implements User {
    Chat chat;

    @Override
    public void sendMsg(String msg) {
        chat.sendMsg(msg, this);
    }

    @Override
    public void getMsg(String msg) {
        System.out.println("User: " + msg);
    }

    public void setChat(Chat chat) {
        this.chat = chat;
    }
}
