package com.grechich.gmail.mediator;

public class Main {
    public static void main(String[] args) {
        User user = new UserImpl();
        User user2 = new UserImpl();
        UserAdmin admin = new UserAdmin();
        Chat chat = new ChatImpl();
        chat.addUser(user);
        chat.addUser(user2);
        chat.setAdmin(admin);
        chat.sendMsg("asda", user);
        }

}
