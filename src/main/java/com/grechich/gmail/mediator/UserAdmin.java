package com.grechich.gmail.mediator;

public class UserAdmin implements User {
    Chat chat;

    @Override
    public void sendMsg(String msg) {
        chat.sendMsg(msg, this);
    }

    @Override
    public void getMsg(String msg) {
        System.out.println("Admin: " + msg);
    }

    public void setChat(Chat chat) {
        this.chat = chat;
    }
}
