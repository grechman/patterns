package com.grechich.gmail.decorator;

public class CustomSender implements Sender{
    private String msg;
    public CustomSender(String msg){
        this.msg = msg;
    }

    @Override
    public void send() {
        System.out.print(msg);
    }
}
