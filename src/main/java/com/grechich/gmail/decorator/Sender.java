package com.grechich.gmail.decorator;

public interface Sender {
    void send();
}
