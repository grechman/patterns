package com.grechich.gmail.decorator;

public class SuperDecoratedSender implements Sender{
    private Sender sender;

    public SuperDecoratedSender(Sender sender) {
        this.sender = sender;
    }

    @Override
    public void send() {
        System.out.print("!!!!!!!!!!!");
        sender.send();
        System.out.print("!!!!!!!!!!");

    }
}

