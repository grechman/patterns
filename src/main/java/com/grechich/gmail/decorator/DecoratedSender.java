package com.grechich.gmail.decorator;

public class DecoratedSender implements Sender {
    private Sender sender;

    public DecoratedSender(Sender sender) {
        this.sender = sender;
    }

    @Override
    public void send() {
        System.out.print("~");
        sender.send();
        System.out.print("~");

    }
}
