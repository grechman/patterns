package com.grechich.gmail.decorator;

public class Main {
    public static void main(String[] args) {


        String str = "Hello!";
        Sender sender = new CustomSender(str);
        Sender decoratedSender = new DecoratedSender(sender);
        Sender superDecoratedSender = new SuperDecoratedSender(sender);
        Sender superPuperSender = new DecoratedSender(new SuperDecoratedSender(sender));
        superPuperSender.send();
    }
}
