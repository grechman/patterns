package com.grechich.gmail.state;

public class PlayStation {
    Game game;

    public void setGame(Game game) {
        this.game = game;
    }

    public void play() {
        if (game==null)
            nextGame();
        game.play();
    }

    public void nextGame() {
        if (game instanceof Fifa) {
            game = new GTA();
        } else if (game instanceof GTA) {
            game = new Fifa();
        }else if (game == null){
            game = new Fifa();
        }
    }


}
