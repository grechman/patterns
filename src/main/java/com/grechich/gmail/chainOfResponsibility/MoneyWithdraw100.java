package com.grechich.gmail.chainOfResponsibility;

public class MoneyWithdraw100 extends MoneyWithdrawaler {

    @Override
    public void givMoney(int value) {
        System.out.println("giving " + value/100 + " by 100");
        if (this.next != null) {
            next.givMoney(value%100);
        }
    }
}