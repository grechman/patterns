package com.grechich.gmail.chainOfResponsibility;

public class MoneyWithdraw500 extends MoneyWithdrawaler {

    @Override
    public void givMoney(int value) {
        System.out.println("giving " + value/500 + " by 500");
        if (this.next != null) {
            next.givMoney(value%500);
        }
    }
}