package com.grechich.gmail.chainOfResponsibility;

public class Main {
    public static void main(String[] args) {
        MoneyWithdrawaler moneyWithdraw1000 = new MoneyWithdraw1000();
        MoneyWithdrawaler moneyWithdraw500 = new MoneyWithdraw500();
        MoneyWithdrawaler moneyWithdraw100 = new MoneyWithdraw100();
        MoneyWithdrawaler moneyWithdraw50 = new MoneyWithdraw50();
        MoneyWithdrawaler moneyWithdraw0 = new MoneyWithdraw0();

        moneyWithdraw1000.setNext(moneyWithdraw500);
        moneyWithdraw500.setNext(moneyWithdraw100);
        moneyWithdraw100.setNext(moneyWithdraw50);
        moneyWithdraw50.setNext(moneyWithdraw0);
        moneyWithdraw1000.givMoney(212150);
    }
}

