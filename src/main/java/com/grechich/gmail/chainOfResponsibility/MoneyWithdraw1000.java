package com.grechich.gmail.chainOfResponsibility;

public class MoneyWithdraw1000 extends MoneyWithdrawaler{

    @Override
    public void givMoney(int value) {
        System.out.println("giving " + value/1000 + " by 1000");
        if (this.next != null) {
            next.givMoney(value%1000);
        }
    }
}