package com.grechich.gmail.chainOfResponsibility;

public abstract class MoneyWithdrawaler {
    public MoneyWithdrawaler next;

    public void setNext(MoneyWithdrawaler next){
        this.next = next;
    }

    public abstract void givMoney(int value);
}
