package com.grechich.gmail.chainOfResponsibility;

public class MoneyWithdraw50 extends MoneyWithdrawaler {

    @Override
    public void givMoney(int value) {
        System.out.println("giving " + value/50 + " by 50");
        if (this.next != null) {
            next.givMoney(value%50);
        }
    }
}